//PSW
//Cps2VKJEXUP9YyQ8KOVbWER
//IDAPP
//bd36731a-3990-488b-9a2a-6946745c61b7

const protocol = "https";
const hostname = "https://skype.seo-business.it";
const port 	   = process.env.port || process.env.PORT || 3798;

var restify = require('restify');
var builder = require('botbuilder');

// Setup Restify Server
var server = restify.createServer();
server.listen(port, hostname, () => {
	//return server.url;
	console.log('%s listening to %s', server.name, server.url); 
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

// Receive messages from the user and respond by echoing each message back (prefixed with 'You said:')
var bot = new builder.UniversalBot(connector, function (session) {
    session.send("Hai scritto: %s", session.message.text);
});